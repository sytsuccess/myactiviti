package com.sun.myactiviti.entity;

import lombok.Data;

import java.util.Date;

/**
 * 〈一句话功能简述〉<br>
 *
 * @author:TT
 * @date 2021/09-24 10:00
 * @Modified BY:
 **/
@Data
public class TaskRes {

	private String processInstanceId;
	private String processDefinitionId;
	private String taskId;
	private String taskName;
	private String businessKey;
	private String assignee;
	private Date createTime;




}
