package com.sun.myactiviti.entity;

import lombok.Data;

import java.util.Date;

@Data
public class TaskResVo {

    private String processInstanceId;
    private String processDefinitionId;
    private String processDefinitionKey;
    private String taskDefinitionKey;
    private String taskId;
    private String taskName;
    private String businessKey;
    private String assignee;
    private Date createTime;


}
