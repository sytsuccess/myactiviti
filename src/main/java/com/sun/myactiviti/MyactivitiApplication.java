package com.sun.myactiviti;

import org.activiti.spring.boot.SecurityAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class MyactivitiApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyactivitiApplication.class, args);
    }

}
