package com.sun.myactiviti.f_processvariables;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * <p></p>
 *
 * @author TT
 * @date 2021-08-08 21:57
 */
@AllArgsConstructor
@Data
public class User implements Serializable {

    private String userId;
    private String username;
}
